commit 2e33efd1684c20a21ec762280997ff691d395e39
Author: reebrando <gma>
Date:   Tue Dec 9 20:01:12 2014 +0000

    Created receiver and sender programs that uses libpackedobjects d to send files.

commit f3aca335bd013f3be4ba270acc5e9bbde600d37a
Author: reebrando <gma>
Date:   Tue Dec 9 13:50:55 2014 +0000

    Implemented validation for all players and booleans fields.

commit db48390b3adf470f8e8ac7bea3c4c696b1fc5557
Author: reebrando <gma>
Date:   Tue Dec 9 12:01:49 2014 +0000

    Fixed input skips with gets() and fflush(stdin). However a better way should be implemented.

commit 50db96e32a080c403a1d959dc5616bafd07f71e6
Author: reebrando <gma>
Date:   Sun Dec 7 21:20:31 2014 +0000

    Added validade.h library which include several functions that validates user input for the basketball xml.

commit a4bf8a98a6cb14d1eba4b4b2e21f2a0f234e27b0
Author: reebrando <gma>
Date:   Fri Dec 5 19:47:21 2014 +0000

    Added size display of the xml after encoding.

commit 4f7a432f2dbf6ccb238c25799b252b8e6f4c6bd0
Author: reebrando <gma>
Date:   Fri Dec 5 19:27:56 2014 +0000

    Changes made in the add_data method and implementation of the insert method which is used to add data to an existing xml data.

commit ef186899b999292b0ac511c73f3d31f75ee89a28
Author: reebrando <gma>
Date:   Fri Dec 5 11:35:54 2014 +0000

    Improved server side with messages that shows what is happening.

commit ddef22ac8df51dc0221beae219e50c1c5abac0ad
Author: reebrando <gma>
Date:   Thu Dec 4 10:54:00 2014 +0000

    Added the client side of the application, that connects to the socket and send data.

commit f7c4a646b1f3164c5a73a8cc37225cdad3261b73
Author: reebrando <gma>
Date:   Fri Nov 28 17:18:37 2014 +0000

    Fixed judges count starting from 1.

commit 54b85875cbe296e44197a98e9e474a21381c4785
Author: reebrando <gma>
Date:   Fri Nov 28 17:11:52 2014 +0000

    Added flexibility to add_data function making posible to adjust number of teams and players through parameters.

commit 5301c306dfa6ca642c745d5ebf0d8e3334dd682a
Author: reebrando <gma>
Date:   Fri Nov 28 11:56:04 2014 +0000

    Updated log file.

commit c93bdac9e0eaab7a8616e5f567614c696b90f1a2
Author: reebrando <gma>
Date:   Fri Nov 28 11:46:29 2014 +0000

    Changes made in stat.c and stat.h file with the new function add_data.

commit 18062892a2af6020ba0af4ed8c8852453e8a48f7
Author: reebrando <gma>
Date:   Fri Nov 21 13:50:26 2014 +0000

    Chaged coach comments.

commit 49ed93ee9a637e680b36dd84e7400afbf7d3f963
Author: reebrando <gma>
Date:   Fri Nov 21 13:45:57 2014 +0000

    Added coach node to main.c.

commit 5081d24f420c09e67b661fd240d34da1cf929491
Author: reebrando <gma>
Date:   Fri Nov 21 11:50:08 2014 +0000

    added encoding and deconding.

commit 98c440d00341397f23c60214ae4e32d4d7c88ed3
Author: reebrando <gma>
Date:   Fri Nov 21 11:24:52 2014 +0000

    Chages added to xml schema to work properly with packedobjects.

commit 5fec5ef0a9104c7cfe4d8868ef136ed711c7e90d
Author: reebrando <gma>
Date:   Mon Nov 17 18:29:05 2014 +0000

    Added the log.txt file

commit 2df649e9cedd226757a1a0120bba4001497cf14c
Author: reebrando <gma>
Date:   Mon Nov 17 18:24:46 2014 +0000

    Added packedobjects references to configure.ac and Makefile.am

commit 3261638e4d3e43d81ed32093884bbe3adc2a24bc
Author: reebrando <gma>
Date:   Mon Nov 17 10:46:01 2014 +0000

    Code was organized and it started to make it user friendly.

commit 925e11e8808257862eaccee5b92202a1b279f2bf
Author: reebrando <gma>
Date:   Fri Nov 14 12:01:35 2014 +0000

    Deleted extra files.

commit a01f66d8063947cce76aaba0887d5328c4554d65
Author: reebrando <gma>
Date:   Fri Nov 14 11:44:56 2014 +0000

    Xml folder with the data and schema added.

commit 5285cc65cfdb7c97d939d34845274603e4c7f02c
Author: reebrando <gma>
Date:   Fri Nov 14 11:44:19 2014 +0000

    statistics added.

commit 53973f97db09335d6e796568a62fabf02c41b018
Author: reebrando <gma>
Date:   Fri Nov 14 11:31:48 2014 +0000

    Changes made in the structure of the project with the use of Autotools.

commit 6413c663303e5b652cb83ad007f01337ce550749
Author: reebrando <gma>
Date:   Fri Nov 14 11:27:59 2014 +0000

    Changes made in the structure of the code with Autotools.

commit d7c2f8acefb6166a7699e6f3bfd5575b2c826cb5
Merge: 89d7bae 89ebfbe
Author: reebrando <gma>
Date:   Fri Nov 7 11:50:55 2014 +0000

    merge branch 'master' of https://bitbucket.org/renanbrando/wireless_applications

commit 89d7bae3d53950be995fb310a757e20916f9ad79
Author: reebrando <gma>
Date:   Fri Nov 7 11:44:05 2014 +0000

    First attempt of using user input after doing the whole static xml.

commit f3c9c729035a7d24a3345b85b25916b4869fdd9c
Author: reebrando <gma>
Date:   Fri Nov 7 11:39:43 2014 +0000

    First attempt to use user input after doing the whole static xml.

commit 89ebfbe08298b360879566cec964d55042804c60
Author: reebrando <gma>
Date:   Mon Oct 27 11:50:20 2014 +0000

    Initial commit with contributors
