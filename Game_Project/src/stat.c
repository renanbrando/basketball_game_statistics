#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stat.h"
#include "validate.h"
#include <packedobjects/packedobjects.h>

char *user_input(char *buffer, char *node_name)
{
    printf("Enter the %s\n", node_name);
    
    if(fgets(buffer, 1000, stdin) == NULL)
    {
        printf("Error\n");
        exit (EXIT_FAILURE);
    }
    strtok(buffer, "\n");
    
    return buffer;
}

int create_data(xmlDocPtr doc, int teams, int players){
  int i, j = 0;
  char buffer[1000];
  xmlNodePtr root_node = NULL,teams_node = NULL, team_node = NULL, players_node = NULL, player_node = NULL, coach_node = NULL, judges_node = NULL, judge_node = NULL;

  printf("============================Basketball Game Statistics Generator===========================\n");
  //root node with statistics
  root_node = xmlNewNode(NULL, BAD_CAST "statistics");
  xmlDocSetRootElement(doc, root_node);

  printf("Please provide the game information.\n");
  //local
  xmlNewChild(root_node, NULL, BAD_CAST "local", BAD_CAST user_input(buffer, "local of the game:"));
  //date
  xmlNewChild(root_node, NULL, BAD_CAST "date", BAD_CAST check_time());
  
  teams_node = xmlNewChild(root_node, NULL, BAD_CAST "teams", NULL);

  for(j = 1; j<=teams; j++){
    printf("Enter information about team number %d.\n", j);
    //team node
    team_node = xmlNewChild(teams_node, NULL, BAD_CAST "team", NULL); 
    // for loop of 1-2 
    
    //teamName 
    xmlNewChild(team_node, NULL, BAD_CAST "teamName", BAD_CAST user_input(buffer, "team name:")); 
    //home
    printf("Type true for yes and false from no.\n");
    xmlNewChild(team_node, NULL, BAD_CAST "home", BAD_CAST check_main(user_input(buffer, "if it is the home team:")));
    
    players_node = xmlNewChild(team_node, NULL, BAD_CAST "players", NULL);
    
    for(i = 1; i <=players; i++) {
      //player node
      player_node = xmlNewChild(players_node, NULL, BAD_CAST "player", NULL);
      printf("\n\nType the following information for the player number %d\n", i);
      //name
      xmlNewChild(player_node, NULL, BAD_CAST "name", BAD_CAST user_input(buffer, "name:"));
      xmlNewChild(player_node, NULL, BAD_CAST "number", BAD_CAST check_range(user_input(buffer, "number:"), 0, 99));
      xmlNewChild(player_node, NULL, BAD_CAST "pts", BAD_CAST check_range(user_input(buffer, "points:"), 0, 999));
      xmlNewChild(player_node, NULL, BAD_CAST "min", BAD_CAST check_range(user_input(buffer, "minutes played:"), 0, 90));
      xmlNewChild(player_node, NULL, BAD_CAST "position", BAD_CAST check_position(user_input(buffer, "position:")));
      xmlNewChild(player_node, NULL, BAD_CAST "reb", BAD_CAST check_range(user_input(buffer, "rebounds:"), 0, 99));
      xmlNewChild(player_node, NULL, BAD_CAST "ast", BAD_CAST check_range(user_input(buffer, "assistences:"), 0, 99));
      xmlNewChild(player_node, NULL, BAD_CAST "pf", BAD_CAST check_range(user_input(buffer, "personal fouls:"), 0, 99));
      xmlNewChild(player_node, NULL, BAD_CAST "st", BAD_CAST check_range(user_input(buffer, "steals:"), 0, 99));
      xmlNewChild(player_node, NULL, BAD_CAST "to", BAD_CAST check_range(user_input(buffer, "turn overs:"), 0, 99));
      xmlNewChild(player_node, NULL, BAD_CAST "blk", BAD_CAST check_range(user_input(buffer, "blocks:"), 0, 99));
    
      //coach node
      printf("\n\nType the following information for the coach.\n");
      //coach node
      coach_node = xmlNewChild(team_node,NULL, BAD_CAST "coach", NULL);
      //coach
      xmlNewChild(coach_node, NULL, BAD_CAST "name", BAD_CAST user_input(buffer, "name:"));
      xmlNewChild(coach_node, NULL, BAD_CAST "main", BAD_CAST check_main(user_input(buffer, "main?")));
        
    }
  }
  //judges node
  judges_node = xmlNewChild(root_node, NULL, BAD_CAST "judges", NULL);
  
  for (i = 1; i<=3; i++){
    printf("\n\nType the following information for the judge number %d.\n", i);
    //judge node
    judge_node = xmlNewChild(judges_node,NULL, BAD_CAST "judge", NULL);
    //judge
    xmlNewChild(judge_node, NULL, BAD_CAST "name", BAD_CAST user_input(buffer, "name:"));
    xmlNewChild(judge_node, NULL, BAD_CAST "main", BAD_CAST check_main(user_input(buffer, "main?")));
  }

  return (EXIT_SUCCESS);
}


int insert_data(xmlDocPtr doc, char *file){
  xmlNodePtr root_node, header_node, item_node;
  char buffer[100];

  printf("Inserting data...\n");
  
  doc = xmlReadFile(file, NULL, 0);
  if (doc == NULL){
    fprintf(stderr, "Failed to parse %s\n", file);
    fflush(stdout);
    return (EXIT_FAILURE);
  }
  
  // dump to console
  root_node = xmlDocGetRootElement(doc);
  
  // tranversing to header node
  xmlNode *cur_node = NULL;
  xmlNode *child_node = NULL;

  for (cur_node = root_node->children; cur_node; cur_node = cur_node->next) {
    printf("1 node type: Element, name: %s\n", cur_node->name);

    if (cur_node->type == XML_ELEMENT_NODE && !xmlStrcmp(cur_node->name, (const xmlChar *)"teams")) {
      printf("2 node type: Element, name: %s\n", cur_node->name);
       
      header_node = cur_node->children;

      for (child_node = header_node->children; child_node; child_node = child_node->next) {
        printf("3 node type: Element, name: %s\n", child_node->name);

        if (child_node->type == XML_ELEMENT_NODE && !xmlStrcmp(child_node->name, (const xmlChar *)"players")) {
          printf("4 node type: Element, name: %s\n", child_node->name);

          // Add a new player to all header nodes
          xmlNodePtr players_node = child_node;
          item_node = xmlNewChild(players_node , NULL, BAD_CAST "player", BAD_CAST NULL);
 
          //player node                                                                                                                                          
          printf("\n\nType the following information for the player new player:\n");
          xmlNewChild(item_node, NULL, BAD_CAST "name", BAD_CAST user_input(buffer, "name:"));
          xmlNewChild(item_node, NULL, BAD_CAST "number", BAD_CAST check_range(user_input(buffer, "number:"), 0, 99));
          xmlNewChild(item_node, NULL, BAD_CAST "pts", BAD_CAST check_range(user_input(buffer, "points:"), 0, 999));
          xmlNewChild(item_node, NULL, BAD_CAST "min", BAD_CAST check_range(user_input(buffer, "minutes played:"), 0, 90));
          xmlNewChild(item_node, NULL, BAD_CAST "position", BAD_CAST check_position(user_input(buffer, "position:")));
          xmlNewChild(item_node, NULL, BAD_CAST "reb", BAD_CAST check_range(user_input(buffer, "rebounds:"), 0, 99));
          xmlNewChild(item_node, NULL, BAD_CAST "ast", BAD_CAST check_range(user_input(buffer, "assistences:"), 0, 99));
          xmlNewChild(item_node, NULL, BAD_CAST "pf", BAD_CAST check_range(user_input(buffer, "personal fouls:"), 0, 99));
          xmlNewChild(item_node, NULL, BAD_CAST "st", BAD_CAST check_range(user_input(buffer, "steals:"), 0, 99));
          xmlNewChild(item_node, NULL, BAD_CAST "to", BAD_CAST check_range(user_input(buffer, "turn overs:"), 0, 99));
          xmlNewChild(item_node, NULL, BAD_CAST "blk", BAD_CAST check_range(user_input(buffer, "blocks:"), 0, 99));

        }
      }
    }     
  }

  // save changes to file
  xmlSaveFormatFileEnc(file, doc, "UTF-8", 1);
  
  //dump to console
   xmlSaveFormatFileEnc("-", doc, "UTF-8", 1);
  return (EXIT_SUCCESS);
}
 
