#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stat.h"
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <packedobjects/packedobjects.h>

#define FILE "game.xml"
#define XML_SCHEMA "game.xsd"

#define HOST_IP "127.0.0.1"
#define HOST_PORT 6969

int main(int argc, char **argv){
  xmlDocPtr doc = NULL;
  packedobjectsContext *pc = NULL;
  char *pdu = NULL;
  char  selection[5] = "n";
  ssize_t bytes_sent;
  int sock;
  struct sockaddr_in servaddr;

  // setup socket
  if ((sock =socket(AF_INET, SOCK_STREAM, 0)) < 0){
    fprintf(stderr, "Error creating socket.\n");
    exit(EXIT_FAILURE);
  }

  // setup addressing
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(HOST_IP);
  servaddr.sin_port = htons(HOST_PORT);

  // initialise packedobjects
  if ((pc = init_packedobjects(XML_SCHEMA, 0, 0)) == NULL) {
    printf("Failed to initialise libpackedobjects.");
    exit(EXIT_FAILURE);
  }

  // menu 
  while (1){
    printf("Type 1 to create a new file, 2 to add content to the existing file or 3 to EXIT:\n");

    if(fgets(selection, 1000, stdin) == NULL)
      {
        printf("Error\n");
        exit (EXIT_FAILURE);
      }
    strtok(selection, "\n");

    if ( strcmp(selection, "1") && strcmp(selection, "2") && strcmp(selection, "3")){
       printf("Error, select and valid option\n");
    }
  
  // create data
  if (!strcmp(selection, "1")){
    doc = xmlNewDoc(BAD_CAST "1.0");
    create_data(doc, 1, 1);
  }
  // insert data
  else if (!strcmp(selection, "2")){
    insert_data(doc, FILE);                                                                                                                            
  }
  else if (!strcmp(selection, "3")){
    printf("Exiting...\n");
    return (EXIT_SUCCESS);
  }
  else {
    continue;
  }

  ////////////////////// Encoding //////////////////////
  
  
  // encode the XML DOM
  pdu = packedobjects_encode(pc, doc);
  if (pc->bytes == -1) {
    printf("Failed to encode with error %d.\n", pc->encode_error);
   }

  // xml size
  printf("Size of XML after encoding %d bytes.\n", pc->bytes);

  // make network connection
  if (connect(sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0){
    fprintf(stderr, "Error calling connect()\n");
    exit(EXIT_FAILURE);
  }

  // send the pdu across the network
  bytes_sent = send(sock, pdu, pc->bytes, 0);

  if (bytes_sent != pc->bytes){
    fprintf(stderr, "Error calling send()\n");
    exit(EXIT_FAILURE);
  }

  if (close(sock) < 0){
    fprintf(stderr, "Error calling close()\n");
    exit(EXIT_FAILURE);
  }

  // free the DOM
  xmlFreeDoc(doc);

  } // end of loop

  ////////////////////// Decoding //////////////////////
  
  // decode the PDU into DOM
  doc = packedobjects_decode(pc, pdu);
  if (pc->decode_error) {
    printf("Failed to decode with error %d.\n", pc->decode_error);
    exit(1);
  }
  
  // output the DOM for checking
  //  packedobjects_dump_doc(doc);
  
  xmlFreeDoc(doc);
  
  // free memory created by packedobjects
  free_packedobjects(pc);

  xmlCleanupParser();
  return(0);

}

