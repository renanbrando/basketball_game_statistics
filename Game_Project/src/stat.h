#include <libxml/parser.h>
#include <libxml/tree.h>

char *user_input(char *buffer, char *node_name);

int create_data(xmlDocPtr doc, int teams, int players);

int insert_data(xmlDocPtr doc, char *file);
